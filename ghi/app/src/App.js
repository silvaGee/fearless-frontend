//old code w/o the tables that contain the data:
// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <div>
//       Number of attendees: {props.attendees.length}
//     </div>
//   );
// }

// export default App;
// import { createRoot } from 'react-dom/client';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav/>
    <div className="my-5 container">
      <Routes>
      <Route index element={<MainPage />} />
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
         <Route path = "attendees" >
        <Route path="" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          {/* Another way to do this is to have the two routes nestled under a parent: PARENT: <Route path = "attendees" />}
          SIBLINGS: <Route path="/" elements={<AttendeesList />} /> <Route path="new" element={<AttendConferenceForm />} /> */}
        </Routes>
      </div>
      </BrowserRouter>
  );
}
export default App;
