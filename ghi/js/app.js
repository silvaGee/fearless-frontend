function createCard(title, description, pictureUrl, location, dateStarts, dateEnds) { //the ' is a template literal
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-subtitle"> ${location}</h6>
        <p class="card-text">${description}</p>
      </div>
    </div>
    <div class="card">
  <div class="card-body">
    <p class="card-text">${dateStarts} - ${dateEnds}</p>
  </div>
</div>
  `;
}
window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.log(e)
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const location = details.conference.location.name;
          const locationDetails = `http://localhost:8000${details.conference.location.href}`;
          const locationDetailResponse = await fetch(locationDetails);
          const dateStarts = new Date(details.conference.starts);
          const newDate = dateStarts.toLocaleDateString()
          const dateEnds = new Date(details.conference.ends);
          const newDateEnds = dateEnds.toLocaleDateString()
          let pictureUrl
          if (locationDetailResponse.ok) {
            const locDetails = await locationDetailResponse.json()
            pictureUrl = locDetails.picture_url;
          }
          const html = createCard(title, description, pictureUrl, location, newDate, newDateEnds);
          const column = document.querySelector('.col');

column.innerHTML += html;

        }
      }

    }
  } catch (e) {
    console.log(e)
  }

});
